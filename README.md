# Invoice discrepancy detector
This is a program that compares the invoice numbers in PDF files with the one stated in an Excel file and identifies any discrepancies between them.

## How to use ?
1. Place the PDF and Excel files you want to compare under the root directory
2. Modify the PDF file name, Excel file name, Excel sheet name, and the target column name to be extracted in lines 6 to 15 of the program.
3. Build a docker image. `docker build -t find-discrepancy .`
4. Run a container from a Docker image. `docker run -it --rm --name invoice-discrepancy find-discrepancy`

