import re
import PyPDF2
import pandas as pd
import numpy as np

# Specify PDF files path
pdf_path_cf = 'FAC_CF_00370833_202311.pdf'
pdf_path_es = 'FAC_ES_00370833_202311.pdf'

# Specify the Excel file path and sheet name
excel_file_path = 'audit2023-11.xlsx'
sheet_name = 'Factures'

# Specify the Excel column name you want to extract
column_to_extract = 'Numéro facture'

def extract_invoice_no_from_pdf(pdf_path):
    invoices = []
    with open(pdf_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfFileReader(file)
        num_pages = pdf_reader.numPages
        
        for page_num in range(num_pages):
            page = pdf_reader.getPage(page_num)
            text = page.extractText()
            
            invoice_pattern = re.compile(r'F\d+/\d+')
            # Extract invoice number based on the new regex pattern
            invoice_matches = invoice_pattern.findall(text)
            invoices.extend(invoice_matches)

    return list(set(invoices))

# Extract invoice numbers from PDF and put them into arrays
invoices_cf = extract_invoice_no_from_pdf(pdf_path_cf)
invoices_es = extract_invoice_no_from_pdf(pdf_path_es)

# Merge 2 array
pdf_array = invoices_cf + invoices_es

# Read the Excel file into a DataFrame
df = pd.read_excel(excel_file_path, sheet_name=sheet_name)

# Extract the specified column
excel_array = df[column_to_extract].tolist()

# Put discrepancy between two arrays using Numpy's setdiff1d Function
array1 = np.array(pdf_array)
array2 = np.array(excel_array)

discrepancy_array1 = [element for element in array1 if element not in array2]
discrepancy_array2 = [element for element in array2 if element not in array1]

print("Elements in PDF but not in Excel:", discrepancy_array1)
print("Elements in Excel but not in PDF:", discrepancy_array2)
